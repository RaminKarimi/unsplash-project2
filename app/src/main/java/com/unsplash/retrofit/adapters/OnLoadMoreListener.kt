package com.unsplash.retrofit.adapters

interface OnLoadMoreListener {
    fun onLoadMoreData()
}
